import { Component, OnInit } from '@angular/core';
import { Planets } from '../model/planets';
import { PlanetsService } from '../services/planets.service';

@Component({
  selector: 'app-planetstable',
  templateUrl: './planetstable.component.html',
  styleUrls: ['./planetstable.component.css']
})
export class PlanetstableComponent implements OnInit {

  planets: Planets[];


  constructor(private planetsService: PlanetsService) { }

  ngOnInit() {
    this.planetsService.findAll().subscribe(
      planetsArray => {
        console.log(planetsArray);
        this.planets = planetsArray.results;
      },
      error => {
        console.log(error);
      }
    );

    
  }

}
