import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetstableComponent } from './planetstable.component';

describe('PlanetstableComponent', () => {
  let component: PlanetstableComponent;
  let fixture: ComponentFixture<PlanetstableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetstableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetstableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
