export class Planets {

    constructor(public name: string,
                public rotation_period: string,
                public orbital_period: string,
                public diameter: number,
                public climate: string,
                ) {}
}
